package com.epam.training.beans;

import java.util.LinkedHashMap;

public class LRUHashMap<K, V> extends LinkedHashMap<K, V> {

    private final int сapacity;

    public LRUHashMap(int сapacity) {
        super(сapacity, 0.80f, true);
        this.сapacity = сapacity;
    }
}
