package com.epam.training.beans;


import com.epam.training.interfaces.Cache;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static sun.plugin2.os.windows.FLASHWINFO.size;

public class LRU<K, V> implements Cache<K, V> {

    private LinkedHashMap<K, V> lru;
    private int capacity;

    public LRU(int initialCapacity) {
        this.capacity = initialCapacity;

        if (capacity == 0) {
            throw new IllegalArgumentException();
        }

        lru = new LRUHashMap<K, V>(initialCapacity){
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return size() > capacity;
            }
        };
    }

    @Override
    public V put(K key, V value) {
        return lru.put(key, value);
    }

    @Override
    public List<Map.Entry<K, V>> getAll() {
        return new LinkedList<>(lru.entrySet());
    }

    @Override
    public V get(K key) {
        return lru.get(key);
    }

    @Override
    public V remove(K key) {
        return lru.remove(key);
    }

    @Override
    public String toString() {
        return lru.toString();
    }
}
