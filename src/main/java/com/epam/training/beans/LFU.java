package com.epam.training.beans;

import com.epam.training.interfaces.Cache;

import java.util.*;

public class LFU<K, V> implements Cache<K, V> {

    private Map<K, V> lfu;
    private int сapacity;
    private Map<K, Integer> freqValues;

    public LFU(int initialCapacity) {
        if (initialCapacity == 0) {
            throw new IllegalArgumentException();
        }
        this.сapacity = initialCapacity;
        lfu = new LinkedHashMap<>(initialCapacity, 0.75f, false);
        freqValues = new HashMap<>();
    }

    @Override
    public String toString() {
        return lfu.toString();
    }

    @Override
    public V remove(K key) {
        freqValues.remove(key);
        return lfu.remove(key);
    }

    @Override
    public List<Map.Entry<K, V>> getAll() {
        Set<K> keys = lfu.keySet();
        for (K key : keys) {
            updateFreq(key);
        }
        return new LinkedList<>(lfu.entrySet());
    }

    @Override
    public V get(K key) {
        V value = lfu.get(key);
        if (value != null) {
            updateFreq(key);
        }
        return lfu.get(key);
    }

    private void updateFreq(K key) {
        Integer freq = freqValues.get(key);
        freqValues.put(key, freq + 1);
    }

    @Override
    public V put(K key, V value) {
        if (lfu.containsKey(key)) {
            updateFreq(key);
        } else {
            if (lfu.size() != сapacity) {
                freqValues.put(key, 0);
            } else {
                int maxFreq = Integer.MAX_VALUE;
                Iterator<Map.Entry<K, Integer>> iterator = freqValues.entrySet().iterator();
                Map.Entry<K, Integer> entry = null;
                while (iterator.hasNext()) {
                    entry = iterator.next();
                    Integer entryFreq = entry.getValue();
                    if (entryFreq < maxFreq) {
                        maxFreq = entryFreq;
                    }
                }
                if (entry != null) {
                    K entryKey = entry.getKey();
                    lfu.remove(entryKey);
                    freqValues.remove(entryKey);
                    freqValues.put(key, 0);
                }
            }
        }
        return lfu.put(key, value);
    }



}
