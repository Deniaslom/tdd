package com.epam.training.interfaces;

import java.util.List;
import java.util.Map;

public interface Cache<K, V> {
    V get(K key);

    List<Map.Entry<K, V>> getAll();

    V put(K key, V value);

    V remove(K key);
}
