package com.epam.training;

import com.epam.training.beans.LFU;
import com.epam.training.interfaces.Cache;
import org.junit.Test;

import static org.junit.Assert.*;

public class LFUTest {
    private Cache<Integer, String> lfuCache;



    @Test
    public void createPutMethod() {
        lfuCache = new LFU<>(5);
        lfuCache.put(1, "1");
        String value = lfuCache.put(1, "2");
        assertEquals("1", value);
    }

    @Test
    public void rightWorkPutMethod() {
        lfuCache = new LFU<>(5);
        lfuCache = new LFU<>(3);
        lfuCache.put(1, "1");
        lfuCache.get(1);
        lfuCache.put(2, "2");
        lfuCache.get(2);
        lfuCache.put(3, "3");
        lfuCache.put(4, "4");
        String value = lfuCache.get(3);
        assertNull(value);
    }

    @Test
    public void createGetMethod() {
        lfuCache = new LFU<>(5);
        lfuCache.put(1, "1");
        String value = lfuCache.get(1);
        assertEquals("1", value);
    }

    @Test
    public void rightWorkGetMethod() {
        lfuCache = new LFU<>(5);
        String value = lfuCache.get(1);
        assertNull(value);
    }

    @Test
    public void createGetAllMethod() {
        lfuCache = new LFU<>(5);
        lfuCache = new LFU<>(3);
        lfuCache.put(1, "1");
        lfuCache.put(2, "2");
        lfuCache.put(3, "3");
        lfuCache.put(4, "4");
        assertEquals(3, lfuCache.getAll().size());
    }

    @Test
    public void createRemoveMethod() {
        lfuCache = new LFU<>(5);
        String value = lfuCache.remove(1);
        assertNull(value);
    }

    @Test
    public void rightWorkRemoveMethod() {
        lfuCache = new LFU<>(5);
        lfuCache.put(1, "1");
        String value = lfuCache.remove(1);
        assertEquals("1", value);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWrongConstructor() {
        lfuCache = new LFU<>(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putWithZeroCapacity() {
        lfuCache = new LFU<>(0);
        lfuCache.put(1, "1");
    }

    @Test
    public void hardTestLFU() {
        lfuCache = new LFU<>(5);
        lfuCache.put(3, "3");
        lfuCache.put(4, "4");
        lfuCache.get(3);
        lfuCache.get(4);
        lfuCache.put(5, "5");
        lfuCache.remove(5);
        lfuCache.put(8, "8");
        lfuCache.put(9, "9");
        lfuCache.get(9);
        lfuCache.put(10, "10");
        lfuCache.put(1, "1");
        lfuCache.get(8);
        lfuCache.get(8);
        lfuCache.get(7);
        lfuCache.put(2, "2");
        assertNull(lfuCache.remove(10));
        assertNull(lfuCache.remove(5));
        assertNotNull(lfuCache.get(3));
        assertNotNull(lfuCache.get(1));
        assertNotNull(lfuCache.get(8));
        assertNotNull(lfuCache.get(4));
        assertNotNull(lfuCache.get(2));
    }
}