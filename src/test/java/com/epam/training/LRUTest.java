package com.epam.training;

import com.epam.training.beans.LRU;
import com.epam.training.interfaces.Cache;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class LRUTest {

    private Cache<String, Integer> lruCache;

    @Test
    public void createRemoveMethod() {
        lruCache.put("1", 1);
        Integer value = lruCache.remove("1");
        assertEquals(1, (int) value);
    }

    @Test
    public void rightWorkRemoveMethod() {
        lruCache.put("1", 1);
        lruCache.put("2", 2);
        lruCache.put("3", 3);
        lruCache.put("4", 4);
        Integer value = lruCache.remove("4");
        assertEquals(4, (int) value);
    }

    @Test
    public void createGetAllMethod() {
        lruCache = new LRU<>(3);
        lruCache.put("1", 1);
        lruCache.put("2", 2);
        lruCache.put("3", 3);
        lruCache.put("4", 4);
        List<Map.Entry<String, Integer>> entries = lruCache.getAll();
        assertEquals(3, entries.size());
    }

    @Test
    public void rightWorkLRUCache() {
        lruCache = new LRU<>(3);
        lruCache.put("1", 1);
        lruCache.put("2", 2);
        lruCache.put("3", 3);
        lruCache.get("1");
        lruCache.put("4", 4);
        assertNull(lruCache.get("2"));
    }

    @Test
    public void putNull() {
        Integer nullValue = lruCache.put(null, 1);
        assertNull(nullValue);
        Integer value = lruCache.put(null, 2);
        assertEquals(1, (int) value);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createWrongConstructor() {
        lruCache = new LRU<>(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putWithZeroCapacity() {
        lruCache = new LRU<>(0);
        lruCache.put("1", 1);
    }

    @Test
    public void fillLRUCache() {
        int capacity = 1;
        int iterValue = 1_000_000;
        int lastKey = iterValue - capacity;
        lruCache = new LRU<>(capacity);
        for (int i = 0; i < iterValue; i++) {
            lruCache.put(i + "", i);
        }
        int returnValue = iterValue - capacity;
        Integer value = lruCache.get(lastKey + "");
        assertEquals(returnValue, (int) value);
    }

    @Test
    public void LRU() {
        lruCache.put("1", 1);
        lruCache.put("2", 2);
        lruCache.put("3", 1);
        lruCache.get("1");
        lruCache.put("4", 1);
        lruCache.put("5", 1);
        lruCache.get("5");
        lruCache.get("3");
        lruCache.put("6", 6);
        lruCache.remove("4");
        lruCache.getAll();
        lruCache.put("7", 7);
        lruCache.get("1");
        lruCache.put("8", 8);
        assertNull(lruCache.get("2"));
        assertNull(lruCache.get("4"));
        assertNotNull(lruCache.get("3"));
        assertNotNull(lruCache.get("6"));
        assertNotNull(lruCache.get("7"));
        assertNotNull(lruCache.get("1"));
        assertNotNull(lruCache.get("8"));
    }
}
